<?php
  require __DIR__ . '/vendor/autoload.php';

  function getYandexMapList($address) {
    $api = new \Yandex\Geo\Api();

    $api->setQuery($address);
    
    $api
        ->setLang(\Yandex\Geo\Api::LANG_RU)
        ->load();
    
    $response = $api->getResponse();
    
    $collection = $response->getList();
    $outList = [];
    $outList['query'] = $address;
    $i = 0;
    foreach ($collection as $item) {
        $outList['list'][$i]['address'] = $item->getAddress();
        $outList['list'][$i]['latitude'] = $item->getLatitude();
        $outList['list'][$i]['longitube'] = $item->getLongitude();
        $i++;
    }
    return $outList;
  }

  $query = '';

  if (!empty($_POST['find'])) {
    $query = $_POST['query'];
    $outList = getYandexMapList($_POST['query']);
  }

  if (!empty($_GET['query'])) {
    $query = $_GET['query'];
    $outList = getYandexMapList($_GET['query']);
  }  
?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Яндекс Карты</title>
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>

  <form action="index.php" method="POST">
    <label for="query">Адрес</label>
    <input type="text" name="query" id="query" value="<?php echo $query ?>">
    <input type="submit" name="find" value="Найти">
  </form>

  <?php if(!empty($outList['list'])): ?>
    <?php foreach($outList['list'] as $item): ?>
      <p>
        <a href="?query=<?php echo $outList['query'] ?>&latitude=<?php echo $item['latitude'] ?>&longitube=<?php echo $item['longitube'] ?>"><?php echo $item['address'] ?></a>
        (широта: <?php echo $item['latitude'] ?>, долгота: <?php echo $item['longitube'] ?>)
      </p>
    <?php endforeach; ?>
  <?php endif; ?>

  <?php if(!empty($_GET['latitude']) and !empty($_GET['longitube'])): ?>
    <div id="map" style="width: 600px; height: 400px"></div>
    <script type="text/javascript">
      ymaps.ready(init);
      function init(){    
          var myMap = new ymaps.Map("map", {
              center: [<?php echo $_GET['latitude'] ?>, <?php echo $_GET['longitube'] ?>],
              zoom: 17
          });
      }
      var myPlacemark = new ymaps.Placemark(<?php echo $_GET['latitude'] ?>, <?php echo $_GET['longitube'] ?>);    
      myMap.geoObjects.add(myPlacemark);
    </script>
  <?php endif; ?>

</body>
</html>